scripts = $(shell find . -maxdepth 1 -name \*.sh -type f)
templates = $(shell find templates -maxdepth 1 -type f -printf "%f\n")
units = $(shell find -maxdepth 1 -name \*.service -or -name \*.timer)
export prefix ?= /usr/local

define TEMPLATE_RULE
$(1): $(2)
	cat $(2) | envsubst > $(1)
endef

.PHONY: all
all: $(templates)

.PHONY: clean
clean:
	rm $(templates)

.PHONY: install
install: $(scripts) $(units)
	install -D -t $(prefix)/bin $(scripts)
	install -D -t $(prefix)/lib/systemd/system $(units)
	install -D -t $(prefix)/lib/systemd/user $(units)


$(foreach _tmpl,$(templates),$(eval $(call TEMPLATE_RULE,$(_tmpl),templates/$(_tmpl))))
