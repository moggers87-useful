#!/bin/bash

# A script to update GoToSocial
#
# Automatically fetches the requested version of GoToSocial, verifies the
# checksum, and unpackages it for you.
#
# Usage: update-gts.sh <version> [<binary_type>]
#
# =============================================================================
# Copyright (c) 2025 Matt Molyneaux <moggers87+git@moggers87.co.uk>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# =============================================================================

set -euxo pipefail

VERSION=$1
BIN_TYPE=${2:-linux_amd64}

if [[ ! -f gotosocial ]]; then
        echo "This script needs to run in the same directory as the gotosocial"
        exit 1;
fi

rm -f checksums.txt gotosocial_*.tar.gz;

wget https://github.com/superseriousbusiness/gotosocial/releases/download/v${VERSION}/gotosocial_${VERSION}_${BIN_TYPE}.tar.gz https://github.com/superseriousbusiness/gotosocial/releases/download/v${VERSION}/checksums.txt

sha256sum --ignore-missing --check checksums.txt

tar xf gotosocial_${VERSION}_${BIN_TYPE}.tar.gz

echo "Now restart gotosocial, e.g. systemctl restart gotosocial"
