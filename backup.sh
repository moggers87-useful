#!/bin/bash

# A script that does backups

# ============================================================================
# Copyright (c) 2019 Matt Molyneaux <moggers87+git@moggers87.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# ============================================================================

# CHANGELOG
#
# 2019-07-31
# * Initial version
#
# 2020-05-19
# * Minor changes to catch errors earlier
#
# 2020-05-21
# * Allow rc_path to be set via environment variable

set -euo pipefail

if [[ -z ${BACKUP_CONFIG:-} ]]; then
    rc_path="~/.config/moggers87/backuprc"
else
    rc_path=$BACKUP_CONFIG
fi

expanded_path=${rc_path/#\~/$HOME}

if [ -e $expanded_path ]; then
    . $expanded_path
fi

if [[ -z ${restic_bin:-} ]]; then
    restic_bin=`which restic`
fi

progname=`basename $0`

sub_restic() {
    $restic_bin $@
}

check_config() {
    if [[ -z ${RESTIC_REPOSITORY:-} ]]; then
        echo "Please set RESTIC_REPOSITORY in $rc_path"
        exit 1
    fi
}

sub_help() {
    echo "$progname [command]"
    echo ""
    echo "Commands:"
    echo ""
    echo "  backup"
    echo "    Start the backup process. Expects environment variables set in $rc_path. Set backup_opts for additional options. See restic docs for more details." | fmt
    echo ""
    echo "  maintenance"
    echo "    Cleans up the repo and removes backups that are no longer required. Expects environment variables set in $rc_path. Set forget_opts and prune_opts for additional options. See restic docs for more details." | fmt
    echo ""
    echo "  restic"
    echo "    Directly call the restic binary. Expects environment variables set in $rc_path" | fmt
    echo ""
    echo "  example"
    echo "    Add an example config file to $rc_path if that path does not already exist" | fmt
    echo ""
}

sub_example() {
    if [ -e $expanded_path ]; then
        echo "$rc_path already exists, refusing to overwrite!"
        return 1
    fi
    mkdir -p `dirname $expanded_path`
    cat <<EOF > $expanded_path
# AWS settings
#export AWS_ACCESS_KEY_ID="access key"
#export AWS_SECRET_ACCESS_KEY="secret key"

# restic environment variables, see restic documentation for details
export RESTIC_REPOSITORY=s3:s3.amazonaws.com/mybucket
export RESTIC_PASSWORD="password"

# options for the backup command, see "restic backup --help" for more options
export backup_opts="--exclude /home/user/.cache --exclude /home/user/unimportant /home/user/"

# options for the maintenance command
# See "restic forget --help"
export forget_opts="--keep-daily 7 --keep-weekly 4 --keep-monthly 3"
# And "restic check --help"
export check_opts=""
# For more options

# Uncomment the following if restic is not in \$PATH
#export restic_bin=/path/to/restic
EOF
}

sub_maintenance() {
    check_config
    $restic_bin unlock
    $restic_bin forget $forget_opts
    $restic_bin check $check_opts
}

sub_backup() {
    check_config
    $restic_bin unlock
    $restic_bin backup $backup_opts
}

subcommand=${1:-}
case $subcommand in
    "" | "-h" | "--help")
        sub_help
        ;;
    *)
        shift
        if [ -n "$(type -t sub_${subcommand})" ]; then
            sub_${subcommand} $@
        else
            echo "Error: '$subcommand' is not a valid command" >&2
            exit 1
        fi
        ;;
esac
