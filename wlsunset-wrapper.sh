#!/bin/bash

# A script that wraps wlsunset

# ============================================================================
# Copyright (c) 2020 Matt Molyneaux <moggers87+git@moggers87.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# ============================================================================

# CHANGELOG
#
# 2020-11-13
# * Initial version
#

set -euo pipefail

# defaults
high_temp=6500
low_temp=3500
latitude=51.8
longitude=0.4
gamma=1.0

if [[ -z ${WLSUNSET_CONFIG:-} ]]; then
    rc_path="~/.config/moggers87/wlsunsetrc"
else
    rc_path=$WLSUNSET_CONFIG
fi

expanded_path=${rc_path/#\~/$HOME}

if [ -e $expanded_path ]; then
    . $expanded_path
fi

if [[ -z ${wlsunset_bin:-} ]]; then
    wlsunset_bin=`which wlsunset`
fi

wlsunset_cmd="$wlsunset_bin -t $low_temp -T $high_temp -l $latitude -L $longitude -g $gamma"
echo $wlsunset_cmd
$wlsunset_cmd
