Useful
======

Some (hopefully) useful scripts.

backup.sh
---------

A wrapper around `restic <https://restic.net/>`__. It mostly just provides a
way to configure restic via an rc file.

shutup.sh
---------

A script to remove merged git branches that I wrote because another developer
would not shut up about how many merged branches hadn't been deleted.

wlsunset-wrapper.sh
-------------------

A script that wraps `wlsunset <https://sr.ht/~kennylevinsen/wlsunset/>`__. It
mostly just provides a way to configure wlsunet via an rc file.
