#!/bin/bash

# A script to delete merged branches in git

# ============================================================================
# Copyright (c) 2016 Matt Molyneaux <moggers87+git@moggers87.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# ============================================================================

# CHANGELOG
#
# 2016-07-09
# * Initial version
#
# 2016-07-10
# * Add ability to specify ignored branches in shutuprc
#
# 2016-07-13
# * Check for shutuprc in user home
#
# 2020-05-19
# * Minor changes to catch errors earlier
#
# 2020-07-01
# * Revert using set -euo pipefail as it breaks things
#
# 2024-11-30
# * Remove --prune from git fetch as it interferes with git-bug

git fetch --all || exit 1

if [ -e ~/.shutuprc ]; then
    . ~/.shutuprc
fi
if [ -e `git rev-parse --show-toplevel`/.shutuprc ]; then
    . `git rev-parse --show-toplevel`/.shutuprc
fi

## these options can be set in `.shutuprc`
if [[ -z ${ignore_branches_regex:-} ]]; then
    ignore_branches_regex="master"
fi

# git diff doesn't support following pipes/symlinks
diff="diff"
which colordiff &> /dev/null && diff="colordiff"

# color support
ncolors=$(tput colors)
if test -n "$ncolors" && test $ncolors -ge 8; then
    # there are more modes available, but we're not interested in them
    bold="$(tput bold)"
    normal="$(tput sgr0)"
fi

current_branch=`git rev-parse --abbrev-ref HEAD`
ignore_branches_regex="($ignore_branches_regex)|$current_branch"
branch_warning="
You are about to remove branches that have been merged into: ${bold}${current_branch}${normal}
Inspect the diff above and then hit return to continue."

# remote branches
remote_branches=`git branch --format="%(refname:lstrip=2)%(symref:short)" -r | sort -n | sed '/^[[:space:]]*$/d'`
remote_branches_deleted=`git branch --format="%(refname:lstrip=2)%(symref:short)" -r --merged | grep -Ev $ignore_branches_regex | sort -n | sed '/^[[:space:]]*$/d'`
if [[ $remote_branches_deleted = *[![:blank:]]* ]]; then
    remote_diff=`grep -vf <(echo "$remote_branches_deleted") <(echo "$remote_branches")`

    echo -e "\nRemote branch changes:"

    $diff -u -Lbefore -Lafter <(echo "$remote_branches") <(echo "$remote_diff")

    echo -e "$branch_warning"
    read
    echo "$remote_branches_deleted" | sed "s/\// /" | xargs  -L 1 git push --delete
else
    echo "No merged remote branches found in $current_branch."
fi

# local branches
local_branches=`git branch --format="%(refname:lstrip=2)" |  sort -n | sed '/^[[:space:]]*$/d'`
local_branches_merged=`git branch --format="%(refname:lstrip=2)" --merged`
local_branches_gone=`git branch --format="%(refname:lstrip=2)%(upstream:track)" --no-merged | grep -E "\[gone\]$"| sed "s/\[gone\]$//"`
local_branches_deleted=`echo -e "$local_branches_merged\n$local_branches_gone" | grep -Ev $ignore_branches_regex | sort -n | sed '/^[[:space:]]*$/d'`
if [[ $local_branches_deleted = *[![:blank:]]* ]]; then
    local_diff=`grep -vf <(echo "$local_branches_deleted") <(echo "$local_branches")`

    echo -e "\nLocal branch changes:"

    $diff -u -Lbefore -Lafter <(echo "$local_branches") <(echo "$local_diff")

    echo -e "$branch_warning"
    read
    echo "$local_branches_deleted" | xargs -L 1 git branch -D
else
    echo "No stale local branches found in $current_branch."
fi
