#!/bin/bash

set -euo pipefail

if [[ -z ${XDG_CACHE_HOME:-} ]]; then
    cache_dir=$HOME/.cache
else
    cache_dir=$XDG_CACHE_HOME
fi

if [[ ! -e $cache_dir ]]; then
    echo "Can't find $cache_dir, exiting..."
    exit 1
fi

if [[ -z ${CLEAN_CACHE_CONFIG:-} ]]; then
    rc_path="~/.config/moggers87/clean_cacherc"
else
    rc_path=$CLEAN_CACHE_CONFIG
fi

expanded_path=${rc_path/#\~/$HOME}

if [ -e $expanded_path ]; then
    . $expanded_path
fi

if [[ -z ${days:-} ]]; then
    days=360
fi

progname=`basename $0`

sub_help() {
    echo "$progname [command]"
    echo ""
    echo "Commands:"
    echo ""
    echo "  clean"
    echo "    Cleans $cache_dir of files that haven't been accessed in the last $days days. Deletes empty directories. Expects environment variables set in $rc_path. Set days to control what is considered old." | fmt
    echo ""
    echo "  find"
    echo "    Same as clean, except it returns the list of files that would be removed rather than actually removing them" | fmt
    echo ""
    echo "  example"
    echo "    Add an example config file to $rc_path if that path does not already exist" | fmt
    echo ""
}

sub_example() {
    if [ -e $expanded_path ]; then
        echo "$rc_path already exists, refusing to overwrite!"
        return 1
    fi
    mkdir -p `dirname $expanded_path`
    cat <<EOF > $expanded_path
# how many days old should something be before it's removed from the cache?
export days=365
EOF
}

sub_clean() {
    find $cache_dir -type f -atime +$days -delete
    find $cache_dir -type d -empty -delete
}

sub_find() {
    find $cache_dir -type f -atime +$days
}

subcommand=${1:-}
case $subcommand in
    "" | "-h" | "--help")
        sub_help
        ;;
    *)
        shift
        if [ -n "$(type -t sub_${subcommand})" ]; then
            sub_${subcommand} $@
        else
            echo "Error: '$subcommand' is not a valid command" >&2
            exit 1
        fi
        ;;
esac
